# Dni otwarte 2023

1. Przedstawienie webdev w szkole 
    - co robimy na zajęciach
    - projekty poszaszkolne (SCI++ i pokazanie przykładowych na projektorze)
    - https://nflowert.github.io/treeGenerator/
    - https://megatotek.onrender.com
    - ewentualnie coś jeszcze od serca
2. Opowiedzenie o zadaniu
   - przedstawiamy generator guzika
3. Zadanie CSS
   - generują styl guzika
   - wklejają kod CSS z generatora do style.css
4. Zadanie JS
   - możliwość wpisania czegokolwiek w alert
   - onClick na zmianę stylu preview-box