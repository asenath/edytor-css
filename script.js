const setStyle = (attributes) => {
    let box = document.querySelector(".box")
    let styles = []
    let boxShadow = ""
    let textShadow = ""
    attributes.forEach((attribute) => {
        let value = !isNaN(attribute.value) ? attribute.value + 'px' : attribute.value
        if (attribute.name.includes("boxShadow")) {
            attribute.name.includes("Color") ? boxShadow += value : boxShadow += value + " "
        } else if (attribute.name.includes("textShadow")) {
            attribute.name.includes("Color") ? textShadow += value : textShadow += value + " "
        } else {
            styles.push([attribute.name, value])
        }
    })
    styles.push(["boxShadow", boxShadow])
    styles.push(["textShadow", textShadow])
    Object.assign(box.style, Object.fromEntries(styles))

    writeCode(styles)
}

const writeCode = (styles) => {
    const code = document.querySelector(".code pre")
    let string = ".box { \n"
    styles.forEach((element) => {
        string += toSnakeCase(element[0]) + ": " + element[1] + "; \n"
    })
    string += "}"
    code.innerHTML = string
}

const toSnakeCase = (str) => {
    return str.replace(/[A-Z]/g, (letter) => {return '-'+ letter.toLowerCase();});
}

const setAttributes = () => {
    let attributes = document.querySelectorAll(".form-input")
    attributes = Object.values(attributes)
    setStyle(attributes)
}

const setJSONAttributes = (json) => {
    obj = Object.entries(JSON.parse(json))

    let phpAttributes = []
    obj.forEach((element) => {
        let temp = {
            name: element[0],
            value: element[1],
        }
        phpAttributes.push(temp)
    })
    setHtml(phpAttributes)
    setStyle(phpAttributes)
}


window.onload = () => {
    setAttributes()   
}

document.addEventListener("input", setAttributes)
